// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

// Use require to add webcamjs
var WebCamera = require("webcamjs");
Caman = require('caman').Caman;

WebCamera.attach('#camdemo');

var fs = require('fs'); // Load the File System to execute our common tasks (CRUD)

// return an object with the processed base64image
function processBase64Image(dataString) {
      var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),response = {};

      if (matches.length !== 3) {
          return new Error('Invalid input string');
      }

      response.type = matches[1];
      response.data = new Buffer(matches[2], 'base64');

      return response;
}

WebCamera.set({
	width: 1280,
	height: 720,
	image_format: 'jpeg',
	jpeg_quality: 100,
	flip_horiz: true
});

document.getElementById("reload").addEventListener('click',function(){
    WebCamera.attach('#camdemo');
});



document.getElementById("savefile").addEventListener('click',function(){

    WebCamera.snap( function(data_uri) {

        document.getElementById('camdemoresult').innerHTML = '<img src="'+data_uri+'"/>';

        Caman('#camdemoresult', function () {
		    this.brightness(100);
		    this.contrast(100);
		    this.sepia(60);
		    this.saturation(-30);
		    this.render();
		  });	

    });
},false);
